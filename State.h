#ifndef STATE_H
#define STATE_H

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <sstream>

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>

class State {
private:
    // Textures vector
    std::vector<sf::Texture> textures;

public:
    // Constructors / Destructors
    State();
    virtual ~State();

    // Functions
    // Pure Virtual functions
    virtual void update() = 0;
    virtual void render() = 0;
};

#endif
