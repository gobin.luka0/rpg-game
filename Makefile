CC = g++

CFLAGS_DEBUG = -Wall -Wextra -std=c++17 -g -O0
CFLAGS_PRODUCTION = -Wall -Wextra -std=c++17 -O3

LIBS = -lsfml-graphics -lsfml-window -lsfml-system

SRCS = main.cpp Game.cpp State.cpp

OBJS = $(SRCS:.cpp=.o)

TARGET = sfml-app

%.o: %.cpp
	$(CC) $(CFLAGS_DEBUG) -c $< -o $@

%.o: %.cpp
	$(CC) $(CFLAGS_PRODUCTION) -c $< -o $@

debug: $(OBJS)
	$(CC) $(OBJS) -o $(TARGET) $(LIBS)

production: $(OBJS)
	$(CC) $(OBJS) -o $(TARGET) $(LIBS)

clean:
	rm -f $(OBJS) $(TARGET)
